package business;

import data.RestaurantSerializator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

/**
 * Class that implements a restaurant together with its staff and actions
 */
public class Restaurant extends Observable implements Serializable {
    RestaurantSerializator serializator;
    /**
     * Class invariant
     */
    private final Administrator admin = new Administrator();
    /**
     * Class invariant
     */
    private final Waiter waiter = new Waiter();
    /**
     * Class invariant
     */
    private final Chef chef = new Chef();
    /**
     * Menu items of the restaurant
     */
    private ArrayList<MenuItem> meniu;
    /**
     * Recorder of all orders made at this restaurant
     */
    private HashMap<Order, ArrayList<MenuItem>> map;

    /**
     * Public constructor of a Restaurant object
     */
    public Restaurant(){
        this.serializator = new RestaurantSerializator();
        this.addObserver(chef);
        this.map = new HashMap<>(100);
        this.meniu = new ArrayList<>();
        this.meniu.add(new BaseProduct("apa", 2.5));
        this.meniu.add(new BaseProduct("paine", 2));
        this.meniu.add(new BaseProduct("suc", 5));
        this.meniu.add(new BaseProduct("inghetata", 3.5));
        this.meniu = serializator.deserialize("restaurant.ser");
    }

    public RestaurantSerializator getSerializator() {
        return serializator;
    }

    /**
     * Retrieves the Waiter of the Restaurant
     * @return a Waiter object
     */
    public Waiter getWaiter() {
        return waiter;
    }

    /**
     * Retrieves the Chef of the Restaurant
     * @return a Chef object
     */
    public Chef getChef() {
        return chef;
    }

    /**
     * Retrieves the register of orders of the restaurant
     * @return all orders of the restaurant
     */
    public HashMap<Order, ArrayList<MenuItem>> getMap() {
        return this.map;
    }

    /**
     * Restores the register of orders
     * @param orders register of orders
     */
    public void setMap(HashMap<Order, ArrayList<MenuItem>> orders) {
        this.map = orders;
    }

    /**
     * Retrieves the menu of the restaurant
     * @return the menu of the restaurant
     */
    public ArrayList<MenuItem> getMeniu() {
        return meniu;
    }

    public void setMeniu(ArrayList<MenuItem> meniu) {
        this.meniu = meniu;
    }

    /**
     * Finds an item in the restaurant menu by name
     * @param name the name of menu item to search for
     * @return the item from the menu with the given name
     */
    public MenuItem findByName(String name){
        for(MenuItem i : this.meniu){
            if(i.getName().equals(name)){
                return i;
            }
        }
        return null;
    }

    /**
     * Adds a new order to the register of the restaurant
     * @param id the id of the new order
     * @param date the date of the new order
     * @param table the table which ordered
     * @param dish the dishes that were ordered
     */
    public void addOrder(int id, String date, int table, ArrayList<MenuItem> dish){
        Order o = waiter.createNewOrder(id, date, table);
        assert (o != null);
        map.put(o, dish);
        setChanged();
        notifyObservers();
    }

    /**
     * Finds the given order in the order register of the restaurant
     * @param o the order to search for
     * @return the menu items ordered through that order
     */
    public ArrayList<MenuItem> findOrder(Order o){
        ArrayList<MenuItem> m = map.get(o);
        assert (m != null);
        return m;
    }

    /**
     * Find an order by id
     * @param idOrder the
     * @return the found order
     */
    public Order findOrderById(int idOrder){
        for(Order i : map.keySet()){
            if(i.getOrderId() == idOrder){
                return i;
            }
        }
        return null;
    }

    /**
     * Creates a new base menu item
     * @param b the base product of the menu item
     */
    public void createMenuItemBase(BaseProduct b){
        BaseProduct aux = admin.createNewBaseMenuItem(b.getName(), b.getPrice());
        assert (aux != null);
        meniu.add(aux);
        assert (meniu != null);
        for(MenuItem i : meniu){
            System.out.println(i.toString());
        }
    }

    /**
     * Creates a new composite menu item
     * @param b the composite product of the menu item
     */
    public void createMenuItemComposite(CompositeProduct b){
        CompositeProduct aux = admin.createNewCompositeMenuItem(b.getName(), b.getIngredients());
        assert (aux != null);
        meniu.add(aux);
        assert (meniu != null);
        for(MenuItem i : meniu){
            System.out.println(i.toString());
        }
    }

    /**
     * Deletes the specified item from the menu
     * @param m the menu item to delete
     */
    public void deleteMenuItem(MenuItem m){
        assert (meniu != null);
        for(MenuItem i : meniu){
            if(m.getName().equals(i.getName())){
                meniu.remove(i);
                break;
            }
        }
        assert (meniu != null);
        for(MenuItem i : meniu){
            System.out.println(i.toString());
        }
    }

    /**
     * Edits the menu item with the specified name
     * @param name the name of the menu item we want to edit
     * @param nou the menu item we want to assign to the edited menu item
     */
    public void editMenuItem(String name, MenuItem nou){
        int t = 0;
        assert (meniu != null);
        for(MenuItem i : meniu){
            if(i.getName().equals(name)){
                meniu.set(t, nou);
            }
            t++;
        }
        assert (meniu != null);
        for(MenuItem i : meniu){
            System.out.println(i.toString());
        }
    }
}
