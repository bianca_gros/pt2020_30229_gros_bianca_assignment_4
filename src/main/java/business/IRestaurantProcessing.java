package business;

import java.util.ArrayList;

/**
 * Interface that groups methods used by the restaurant staff
 */
public interface IRestaurantProcessing {
    /**
     * Method that creates a new composite menu item
     * @pre name != null
     * @pre ingred != null
     * @post for Restaurant r getMeniu() != null
     * @param name the name of the new composite menu item
     * @param ingred the list of ingredients of that new composite menu item
     * @return the new composite menu item
     */
    public CompositeProduct createNewCompositeMenuItem(String name, ArrayList<BaseProduct> ingred);

    /**
     * Method that creates a new base menu item
     * @pre name != null
     * @pre price != 0
     * @pre price is a positive double
     * @post for Restaurant r getMeniu() != null
     * @param name the name of the new base menu item
     * @param price the price of the new base menu item
     * @return the new base menu item
     */
    public BaseProduct createNewBaseMenuItem(String name, double price);

    /**
     * Method that deletes a specified menu item
     * @pre name != null
     * @param name the name of the menu item we want to delete
     * @return the menu item we want to delete
     */
    public MenuItem deleteMenuItem(String name);

    /**
     * Method that edits a specified menu item
     * @pre name != null
     * @param name the name of the menu item we want to edit
     * @return the menu item we want to delete
     */
    public MenuItem editMenuItem(String name);

    /**
     * Method that creates a new order
     * @pre orderId != 0
     * @pre date is a string that represents a date
     * @pre date != null
     * @pre table != 0
     * @post for restaurant r getMap() != null
     * @param orderId the id for the order to be inserted
     * @param date the date of the order
     * @param table the number of the table for which the order is
     * @return the order just created
     */
    public Order createNewOrder(int orderId, String date, int table);

    /**
     * Method that computes price for a set of menu items
     * @pre items != null
     * @post result of method is not null
     * @param items the set of menu items we want to compute price for
     * @return the price of the order
     */
    public double computeOrderPrice(ArrayList<MenuItem> items);

    /**
     * Method that generates a bill for a specific order in .txt format
     * @pre orderId != 0
     * @pre orderId is a valid order ID
     * @param orderId the id for the order to be billed
     * @param restaurant the current restaurant
     */
    public void generateBill(int orderId, Restaurant restaurant);
}
