package business;

import javax.swing.*;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Chef implements Observer, Serializable {

    private JFrame frame;
    public Chef(){
        this.frame= new JFrame();
    }

    @Override
    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(frame, "The cook has just been notified about your order.\nThe food is on the way!\n",
                "It's time to cook!", JOptionPane.PLAIN_MESSAGE, null);
    }
}
