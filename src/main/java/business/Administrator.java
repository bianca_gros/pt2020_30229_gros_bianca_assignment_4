package business;

import java.io.Serializable;
import java.util.ArrayList;

public class Administrator implements IRestaurantProcessing, Serializable {

    @Override
    public BaseProduct createNewBaseMenuItem(String name, double price) {
        BaseProduct newBaseProduct = new BaseProduct(name, price);
        return newBaseProduct;
    }

    @Override
    public CompositeProduct createNewCompositeMenuItem(String name, ArrayList<BaseProduct> ingred) {
        CompositeProduct newCompositeProduct = new CompositeProduct(name, ingred);
        return newCompositeProduct;
    }

    @Override
    public MenuItem deleteMenuItem(String name) {
        MenuItem m = new MenuItem(name);
        return m;
    }

    @Override
    public MenuItem editMenuItem(String name) {
        MenuItem item = new MenuItem(name);
        return item;
    }

    @Override
    public Order createNewOrder(int orderId, String date, int table){
        return null;
    }

    @Override
    public double computeOrderPrice(ArrayList<MenuItem> items){
        return 0;
    }

    @Override
    public void generateBill(int orderId, Restaurant restaurant) {
    }
}
