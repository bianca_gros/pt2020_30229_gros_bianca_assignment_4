package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Order implements Serializable {
    private int orderId;
    private String orderDate;
    private int table;
    private ArrayList<MenuItem> dishes;

    public Order(){

    }

    public Order(int orderId, String orderDate, int table){
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.table = table;
    }

    public Order(int orderId, String orderDate, int table, ArrayList<MenuItem> dishes){
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.table = table;
        this.dishes = dishes;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public ArrayList<MenuItem> getDishes() {
        return dishes;
    }

    public void setDishes(ArrayList<MenuItem> dishes) {
        this.dishes = dishes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getOrderId() == order.getOrderId() &&
                getTable() == order.getTable() &&
                getOrderDate().equals(order.getOrderDate());
    }

    @Override
    public int hashCode() {
       return Objects.hash(getOrderId(), getOrderDate(), getTable());
    }


}
