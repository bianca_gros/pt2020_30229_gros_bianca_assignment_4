package business;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Serializable {

    private ArrayList<BaseProduct> ingredients;

    public CompositeProduct(){
        super();
        this.ingredients = new ArrayList<BaseProduct>();
    }

    public CompositeProduct(String name, ArrayList<BaseProduct> ingredients){
        super(name);
        double p = 0;
        this.ingredients = ingredients;
        for(BaseProduct b : this.getIngredients()){
            p += b.getPrice();
        }
        super.setPrice(p);

    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        super.setName(name);
    }

    public double getPrice() {
        double p = 0;
        for(BaseProduct b : this.getIngredients()){
            p += b.getPrice();
        }
        return p;
    }

    public void setPrice(double price) {
        super.setPrice(price);
    }

    public ArrayList<BaseProduct> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<BaseProduct> ingredients) {
        this.ingredients = ingredients;
    }

    public double computePrice(){
        double pr = 0;
        for(BaseProduct b : this.getIngredients()){
            pr += b.getPrice();
        }
        return pr;
    }
}
