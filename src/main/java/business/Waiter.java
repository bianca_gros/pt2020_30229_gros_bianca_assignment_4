package business;

import data.FileWriterClass;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Waiter implements IRestaurantProcessing, Serializable {

    public Order createNewOrder(int orderId, String date, int table){
        Order newOrder = new Order(orderId, date, table);
        return newOrder;
    }

    @Override
    public double computeOrderPrice(ArrayList<MenuItem> items) {
        double total = 0;
        for(MenuItem i : items){
            total += i.getPrice();
        }
        return total;
    }

    @Override
    public void generateBill(int orderId, Restaurant r) {
        Order com = r.findOrderById(orderId);
        if(com != null){
            FileWriterClass w = new FileWriterClass();
            w.write(com, r);
        }

    }

    @Override
    public CompositeProduct createNewCompositeMenuItem(String name, ArrayList<BaseProduct> ingred) {
        return null;
    }

    @Override
    public BaseProduct createNewBaseMenuItem(String name, double price) {
        return null;
    }

    @Override
    public MenuItem deleteMenuItem(String name) {
        return null;
    }

    @Override
    public MenuItem editMenuItem(String name) {
        return null;
    }

}
