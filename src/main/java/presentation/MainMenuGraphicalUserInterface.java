package presentation;

import javax.swing.*;
import java.awt.*;

public class MainMenuGraphicalUserInterface {
    private JFrame frame;
    private JPanel panel;
    private JPanel textPanel1;
    private JPanel textPanel2;
    private JPanel buttonPanel;
    /*private JPanel savePanel;*/
    private JLabel text1;
    private JLabel text2;
    private JButton adminButton;
    private JButton waiterButton;
    /*private JButton save;
    private JButton deserialize;*/

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getTextPanel1() {
        return textPanel1;
    }

    public void setTextPanel1(JPanel textPanel1) {
        this.textPanel1 = textPanel1;
    }

    public JPanel getTextPanel2() {
        return textPanel2;
    }

    public void setTextPanel2(JPanel textPanel2) {
        this.textPanel2 = textPanel2;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public void setButtonPanel(JPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    public JLabel getText1() {
        return text1;
    }

    public void setText1(JLabel text1) {
        this.text1 = text1;
    }

    public JLabel getText2() {
        return text2;
    }

    public void setText2(JLabel text2) {
        this.text2 = text2;
    }

    public JButton getAdminButton() {
        return adminButton;
    }

    public void setAdminButton(JButton adminButton) {
        this.adminButton = adminButton;
    }

    public JButton getWaiterButton() {
        return waiterButton;
    }

    public void setWaiterButton(JButton waiterButton) {
        this.waiterButton = waiterButton;
    }

    /*public JPanel getSavePanel() {
        return savePanel;
    }

    public void setSavePanel(JPanel savePanel) {
        this.savePanel = savePanel;
    }

    public JButton getSave() {
        return save;
    }

    public void setSave(JButton save) {
        this.save = save;
    }

    public JButton getDeserialize() {
        return deserialize;
    }

    public void setDeserialize(JButton deserialize) {
        this.deserialize = deserialize;
    }*/

    public MainMenuGraphicalUserInterface(String name){
        this.frame = new JFrame("Main Menu");
        this.panel = new JPanel();
        this.textPanel1 = new JPanel();
        this.textPanel2 = new JPanel();
        this.buttonPanel = new JPanel();
        //this.savePanel = new JPanel();
        this.text1 = new JLabel("Welcome to the restaurant!");
        this.text1.setFont(new Font("Britannic Bold", Font.BOLD,24));
        this.text2 = new JLabel("Choose your role:");
        this.text2.setFont(new Font("Calibri", Font.PLAIN, 12));
        this.adminButton = new JButton("Administrator");
        this.adminButton.setFont(new Font("Britannic Bold", Font.PLAIN, 12));
        this.waiterButton = new JButton("    Waiter    ");
        this.waiterButton.setFont(new Font("Britannic Bold", Font.PLAIN, 12));
        /*this.save = new JButton("Serialize Restaurant");
        this.save.setFont(new Font("Britannic Bold", Font.PLAIN, 12));
        this.deserialize = new JButton(("Deserialize Restaurant"));*/

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        frame.add(panel);
        panel.add(textPanel1);
        panel.add(textPanel2);
        panel.add(buttonPanel);
        //panel.add(savePanel);

        textPanel1.add(text1);
        textPanel2.add(text2);
        buttonPanel.add(adminButton);
        buttonPanel.add(waiterButton);
        buttonPanel.setLayout(new GridBagLayout());
        /*savePanel.add(save);
        savePanel.add(deserialize);
        savePanel.setLayout(new GridBagLayout());
*/
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        frame.setVisible(true);
    }
}
