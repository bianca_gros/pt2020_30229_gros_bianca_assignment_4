package presentation;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class EditCompositeProduct {

    private JFrame frame;
    private JPanel panel;
    private JPanel panel1;
    private JPanel panel2;
    private JButton save;
    private JLabel inf;
    private JLabel name;
    private JLabel numele;
    private JLabel ingredients;
    private JTextField i;

    public EditCompositeProduct(String n){
        this.frame = new JFrame();
        this.panel = new JPanel();
        this.panel1 = new JPanel();
        this.panel2 = new JPanel();
        this.save = new JButton("Save changes");
        this.inf = new JLabel("                          Modify the information about the product");
        this.name = new JLabel("Name: ");
        this.numele = new JLabel(n);
        this.ingredients = new JLabel("Ingredients: ");
        this.i = new JTextField(10);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        panel.add(inf);
        panel.add(panel1);
        panel.add(panel2);

        panel1.add(name);
        panel1.add(numele);
        panel2.add(ingredients);
        panel2.add(i);
        panel1.setLayout(new GridBagLayout());
        panel2.setLayout(new GridBagLayout());
        panel.add(save);
        frame.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JPanel getPanel2() {
        return panel2;
    }

    public void setPanel2(JPanel panel2) {
        this.panel2 = panel2;
    }

    public JButton getSave() {
        return save;
    }

    public void setSave(JButton save) {
        this.save = save;
    }

    public JLabel getInf() {
        return inf;
    }

    public void setInf(JLabel inf) {
        this.inf = inf;
    }

    public JLabel getName() {
        return name;
    }

    public void setName(JLabel name) {
        this.name = name;
    }

    public JLabel getNumele() {
        return numele;
    }

    public void setNumele(JLabel numele) {
        this.numele = numele;
    }

    public JLabel getIngredients() {
        return ingredients;
    }

    public void setIngredients(JLabel ingredients) {
        this.ingredients = ingredients;
    }

    public JTextField getI() {
        return i;
    }

    public void setI(JTextField i) {
        this.i = i;
    }
}
