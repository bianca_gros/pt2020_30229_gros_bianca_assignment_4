package presentation;

import business.Administrator;
import business.BaseProduct;
import business.CompositeProduct;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class NewCompositeProduct {
    private JFrame frame;
    private JPanel panel;
    private JPanel textPanel;
    private JPanel buttonPanel;
    private JButton save;
    private JTextField text1;

    private JTextField text3;
    private JLabel labelName;

    private JLabel labelIngredients;
    private Administrator admin;

    public NewCompositeProduct() {
        this.admin = new Administrator();
        this.frame = new JFrame("Create e new composite menu item");
        this.panel = new JPanel();
        this.textPanel = new JPanel();
        this.buttonPanel = new JPanel();
        this.save = new JButton("Save changes");
        this.text1 = new JTextField(16);

        this.text3 = new JTextField(30);
        text1.setColumns(16);

        text3.setColumns(30);
        this.labelName = new JLabel("Menu item name: ");

        this.labelIngredients = new JLabel("Ingredients: ");


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        textPanel.add(labelName);
        textPanel.add(text1);

        textPanel.add(labelIngredients);
        textPanel.add(text3);
        panel.add(textPanel);
        buttonPanel.add(save);
        panel.add(buttonPanel);
        textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
        buttonPanel.setLayout(new GridBagLayout());
        frame.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getTextPanel() {
        return textPanel;
    }

    public void setTextPanel(JPanel textPanel) {
        this.textPanel = textPanel;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public void setButtonPanel(JPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    public JButton getSave() {
        return save;
    }

    public void setSave(JButton save) {
        this.save = save;
    }

    public JTextField getText1() {
        return text1;
    }

    public void setText1(JTextField text1) {
        this.text1 = text1;
    }

    public JTextField getText3() {
        return text3;
    }

    public void setText3(JTextField text3) {
        this.text3 = text3;
    }

    public JLabel getLabelName() {
        return labelName;
    }

    public void setLabelName(JLabel labelName) {
        this.labelName = labelName;
    }

    public JLabel getLabelIngredients() {
        return labelIngredients;
    }

    public void setLabelIngredients(JLabel labelIngredients) {
        this.labelIngredients = labelIngredients;
    }
}
