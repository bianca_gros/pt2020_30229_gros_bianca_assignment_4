package presentation;

import business.Administrator;
import business.BaseProduct;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class NewBaseProduct {
    private JFrame frame;
    private JPanel panel;
    private JPanel textPanel;
    private JPanel buttonPanel;
    private JButton save;
    private JTextField text1;
    private JTextField text2;
    private JLabel labelName;
    private JLabel labelPrice;
    private Administrator admin;

    public NewBaseProduct(){
        this.admin = new Administrator();
        this.frame = new JFrame("Create e new base menu item");
        this.panel = new JPanel();
        this.textPanel = new JPanel();
        this.buttonPanel = new JPanel();
        this.save = new JButton("Save changes");
        this.text1 = new JTextField(16);
        this.text2 = new JTextField(10);
        this.labelName = new JLabel("Menu item name:   ");
        this.labelPrice = new JLabel("        Price:   ");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        panel.add(textPanel);
        panel.add(buttonPanel);
        buttonPanel.add(save);
        textPanel.add(labelName);
        textPanel.add(text1);
        textPanel.add(labelPrice);
        textPanel.add(text2);
        textPanel.setLayout(new GridBagLayout());
        buttonPanel.setLayout(new GridBagLayout());
        frame.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getTextPanel() {
        return textPanel;
    }

    public void setTextPanel(JPanel textPanel) {
        this.textPanel = textPanel;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public void setButtonPanel(JPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    public JButton getSave() {
        return save;
    }

    public void setSave(JButton save) {
        this.save = save;
    }

    public JTextField getText1() {
        return text1;
    }

    public void setText1(JTextField text1) {
        this.text1 = text1;
    }

    public JTextField getText2() {
        return text2;
    }

    public void setText2(JTextField text2) {
        this.text2 = text2;
    }

    public JLabel getLabelName() {
        return labelName;
    }

    public void setLabelName(JLabel labelName) {
        this.labelName = labelName;
    }

    public JLabel getLabelPrice() {
        return labelPrice;
    }

    public void setLabelPrice(JLabel labelPrice) {
        this.labelPrice = labelPrice;
    }
}
