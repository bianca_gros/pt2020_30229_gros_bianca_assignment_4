package presentation;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class WaiterGraphicalUserInterface {

    private JFrame frame;
    private JPanel panel;
    private JPanel textPanel;
    private JPanel buttonPanel;
    private JButton createButton;
    private JButton priceButton;
    private JButton billButton;
    private JButton listButton;

    public WaiterGraphicalUserInterface(){
        this.frame = new JFrame("Waiter Menu");
        this.panel = new JPanel();
        this.textPanel = new JPanel();
        this.buttonPanel = new JPanel();
        this.createButton = new JButton("Create new order");
        this.priceButton = new JButton("Compute price for order");
        this.billButton = new JButton("Generate bill");
        this.listButton = new JButton("View all orders");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        panel.add(textPanel);
        panel.add(buttonPanel);
        buttonPanel.add(createButton);
        buttonPanel.add(priceButton);
        buttonPanel.add(billButton);
        buttonPanel.add(listButton);
        buttonPanel.setLayout(new GridBagLayout());
        frame.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getTextPanel() {
        return textPanel;
    }

    public void setTextPanel(JPanel textPanel) {
        this.textPanel = textPanel;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public void setButtonPanel(JPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    public JButton getCreateButton() {
        return createButton;
    }

    public void setCreateButton(JButton createButton) {
        this.createButton = createButton;
    }

    public JButton getPriceButton() {
        return priceButton;
    }

    public void setPriceButton(JButton priceButton) {
        this.priceButton = priceButton;
    }

    public JButton getBillButton() {
        return billButton;
    }

    public void setBillButton(JButton billButton) {
        this.billButton = billButton;
    }

    public JButton getListButton() {
        return listButton;
    }

    public void setListButton(JButton listButton) {
        this.listButton = listButton;
    }
}
