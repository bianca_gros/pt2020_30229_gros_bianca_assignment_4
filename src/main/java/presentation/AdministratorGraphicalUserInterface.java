package presentation;

import business.Administrator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class AdministratorGraphicalUserInterface {
    private JFrame frame;
    private JPanel panel;
    private JPanel textPanel;
    private JPanel buttonPanel;
    private JButton addButton;
    private JButton deleteButton;
    private JButton modifyButton;
    private JButton listButton;

    public AdministratorGraphicalUserInterface(){

        this.frame = new JFrame("Admin Menu");
        this.panel = new JPanel();
        this.textPanel = new JPanel();
        this.buttonPanel = new JPanel();
        this.addButton = new JButton("Add new item to the menu");
        this.deleteButton = new JButton("Delete item from the menu");
        this.modifyButton = new JButton("Modify item from the menu");
        this.listButton = new JButton("View all menu items");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(750,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        panel.add(textPanel);
        panel.add(buttonPanel);
        buttonPanel.add(addButton);
        buttonPanel.add(deleteButton);
        buttonPanel.add(modifyButton);
        buttonPanel.add(listButton);
        buttonPanel.setLayout(new GridBagLayout());

        frame.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getTextPanel() {
        return textPanel;
    }

    public void setTextPanel(JPanel textPanel) {
        this.textPanel = textPanel;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public void setButtonPanel(JPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public void setAddButton(JButton addButton) {
        this.addButton = addButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(JButton deleteButton) {
        this.deleteButton = deleteButton;
    }

    public JButton getModifyButton() {
        return modifyButton;
    }

    public void setModifyButton(JButton modifyButton) {
        this.modifyButton = modifyButton;
    }

    public JButton getListButton() {
        return listButton;
    }

    public void setListButton(JButton listButton) {
        this.listButton = listButton;
    }


}
