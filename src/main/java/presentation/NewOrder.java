package presentation;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class NewOrder {
    private JFrame frame;
    private JPanel panel;
    private JPanel panelId;
    private JPanel panelDate;
    private JPanel panelTable;
    private JPanel panelDishes;
    private JPanel panelButton;
    private JLabel labelInformation;
    private JLabel labelId;
    private JLabel labelDate;
    private JLabel labelTable;
    private JLabel labelDishes;
    private JTextField textId;
    private JTextField textDate;
    private JTextField textTable;
    private JTextField textDishes;
    private JButton save;

    public NewOrder(){
        this.frame = new JFrame();
        this.panel = new JPanel();
        this.panelId = new JPanel();
        this.panelDate = new JPanel();
        this.panelTable = new JPanel();
        this.panelDishes = new JPanel();
        this.labelInformation = new JLabel("Enter information about the new order: ");
        this.labelId = new JLabel("ID order: ");
        this.labelDate = new JLabel("Date: ");
        this.labelTable = new JLabel("Table no: ");
        this.labelDishes = new JLabel("Ordered dishes: ");
        this.textId = new JTextField(12);
        this.textDate = new JTextField(14);
        this.textTable = new JTextField(12);
        this.textDishes = new JTextField(30);
        this.save = new JButton("Save changes");

        panel.add(labelInformation);
        panelId.add(labelId);
        panelId.add(textId);
        panelId.setLayout(new GridBagLayout());

        panelDate.add(labelDate);
        panelDate.add(textDate);
        panelDate.setLayout(new GridBagLayout());

        panelTable.add(labelTable);
        panelTable.add(textTable);
        panelTable.setLayout(new GridBagLayout());

        panelDishes.add(labelDishes);
        panelDishes.add(textDishes);
        panelDishes.setLayout(new GridBagLayout());

        panel.add(panelId);
        panel.add(panelDate);
        panel.add(panelTable);
        panel.add(panelDishes);
        panel.add(save);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.add(panel);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getPanelId() {
        return panelId;
    }

    public void setPanelId(JPanel panelId) {
        this.panelId = panelId;
    }

    public JPanel getPanelDate() {
        return panelDate;
    }

    public void setPanelDate(JPanel panelDate) {
        this.panelDate = panelDate;
    }

    public JPanel getPanelTable() {
        return panelTable;
    }

    public void setPanelTable(JPanel panelTable) {
        this.panelTable = panelTable;
    }

    public JPanel getPanelDishes() {
        return panelDishes;
    }

    public void setPanelDishes(JPanel panelDishes) {
        this.panelDishes = panelDishes;
    }

    public JPanel getPanelButton() {
        return panelButton;
    }

    public void setPanelButton(JPanel panelButton) {
        this.panelButton = panelButton;
    }

    public JLabel getLabelInformation() {
        return labelInformation;
    }

    public void setLabelInformation(JLabel labelInformation) {
        this.labelInformation = labelInformation;
    }

    public JLabel getLabelId() {
        return labelId;
    }

    public void setLabelId(JLabel labelId) {
        this.labelId = labelId;
    }

    public JLabel getLabelDate() {
        return labelDate;
    }

    public void setLabelDate(JLabel labelDate) {
        this.labelDate = labelDate;
    }

    public JLabel getLabelTable() {
        return labelTable;
    }

    public void setLabelTable(JLabel labelTable) {
        this.labelTable = labelTable;
    }

    public JLabel getLabelDishes() {
        return labelDishes;
    }

    public void setLabelDishes(JLabel labelDishes) {
        this.labelDishes = labelDishes;
    }

    public JTextField getTextId() {
        return textId;
    }

    public void setTextId(JTextField textId) {
        this.textId = textId;
    }

    public JTextField getTextDate() {
        return textDate;
    }

    public void setTextDate(JTextField textDate) {
        this.textDate = textDate;
    }

    public JTextField getTextTable() {
        return textTable;
    }

    public void setTextTable(JTextField textTable) {
        this.textTable = textTable;
    }

    public JTextField getTextDishes() {
        return textDishes;
    }

    public void setTextDishes(JTextField textDishes) {
        this.textDishes = textDishes;
    }

    public JButton getSave() {
        return save;
    }

    public void setSave(JButton save) {
        this.save = save;
    }
}
