package presentation;

import business.MenuItem;
import business.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class ListMenuItems {
    private JFrame frame;
    private JPanel panel;
    private JTable table;
    DefaultTableModel tableModel;

    public ListMenuItems(Restaurant r){
        this.frame = new JFrame();
        this.panel = new JPanel();
        this.tableModel = new DefaultTableModel();
        this.table = new JTable(tableModel);

        tableModel.addColumn("Menu item");
        tableModel.addColumn("Price");

        for(MenuItem i : r.getMeniu()){
            tableModel.insertRow(tableModel.getRowCount(), new Object[] { i.getName().toString(), i.getPrice()});

        }

        panel.add(table);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(550, 350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.add(panel);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.add(new JScrollPane(table));
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
