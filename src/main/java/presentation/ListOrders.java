package presentation;

import business.MenuItem;
import business.Order;
import business.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.Map;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class ListOrders {
    private JFrame frame;
    private JPanel panel;
    private JTable table;
    DefaultTableModel tableModel;

    public ListOrders(Restaurant r){
        this.frame = new JFrame();
        this.panel = new JPanel();
        this.tableModel = new DefaultTableModel();
        this.table = new JTable(tableModel);

        tableModel.addColumn("Order ID");
        tableModel.addColumn("Date");
        tableModel.addColumn("Table");
        tableModel.addColumn("Dishes");

        for (Map.Entry<Order, ArrayList<MenuItem>> entry : r.getMap().entrySet()) {
            tableModel.insertRow(tableModel.getRowCount(), new Object[] { entry.getKey().getOrderId(), entry.getKey().getOrderDate(), entry.getKey().getTable(), entry.getValue()});
        }

        panel.add(table);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(550,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.add(panel);
        frame.setContentPane(panel);
        frame.add(new JScrollPane(table));
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
