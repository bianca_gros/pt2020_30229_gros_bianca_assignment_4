package presentation;

import business.*;
import data.RestaurantSerializator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class MainControl {
    private MainMenuGraphicalUserInterface control = new MainMenuGraphicalUserInterface("Controller");
    private Restaurant r = new Restaurant();

    public MainControl(String fileName){
        control.getAdminButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdministratorGraphicalUserInterface a = new AdministratorGraphicalUserInterface();
                Administrator admin = new Administrator();

                a.getAddButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object[] possibilities = {"base product", "composite product"};
                        String s = (String)JOptionPane.showInputDialog(
                                a.getFrame(),
                                "Choose the type of menu item you want to add:\n",
                                "Type of menu item",
                                JOptionPane.PLAIN_MESSAGE,
                                null,
                                possibilities,
                                "ham");
                        if (s.equals("base product")){
                            NewBaseProduct n = new NewBaseProduct();
                            n.getSave().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    if(n.getText1().getText() != null && n.getText2().getText() != null){
                                        String name = n.getText1().getText();
                                        String p = n.getText2().getText();
                                        double price = Double.parseDouble(p);
                                        r.createMenuItemBase(admin.createNewBaseMenuItem(name,price));
                                        JOptionPane.showMessageDialog(a.getFrame(), "Item inserted in the menu");
                                        r.getSerializator().serialize(r.getMeniu(), fileName);
                                    }
                                }
                            });
                        }else{
                            NewCompositeProduct c = new NewCompositeProduct();
                            c.getSave().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    if(c.getText1().getText() != null && c.getText3().getText() != null){
                                        String name = c.getText1().getText();
                                        String i = c.getText3().getText();
                                        String[] arrOfStr = i.split(", ");
                                        ArrayList<BaseProduct> ingredients = new ArrayList<>();
                                        for(String aux : arrOfStr){
                                            BaseProduct a = (BaseProduct) r.findByName(aux);
                                            if(a == null){
                                                JOptionPane.showMessageDialog(c.getFrame(),
                                                        "Error! Enter only ingredients from the menu");
                                                break;
                                            }
                                            else{
                                                ingredients.add(a);
                                            }
                                        }
                                        if(ingredients.size()== arrOfStr.length){
                                            r.createMenuItemComposite(admin.createNewCompositeMenuItem(name,ingredients));
                                            JOptionPane.showMessageDialog(a.getFrame(), "Item inserted in the menu");
                                            r.getSerializator().serialize(r.getMeniu(), fileName);
                                        }
                                    }
                                }
                            });
                        }
                    }
                });

                a.getDeleteButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String s = (String)JOptionPane.showInputDialog(
                                a.getFrame(),
                                "Enter the name of the menu item you want to delete:\n",
                                "Name of menu item",
                                JOptionPane.PLAIN_MESSAGE,
                                null,
                                null,
                                "");
                        if(s == null){
                            JOptionPane.showMessageDialog(a.getFrame(),
                                    "Error! Invalid data");
                        }
                        else{
                            r.deleteMenuItem(admin.deleteMenuItem(s));
                            JOptionPane.showMessageDialog(a.getFrame(), "Item deleted");
                            r.getSerializator().serialize(r.getMeniu(), fileName);
                        }
                    }
                });

                a.getModifyButton().addActionListener(new ActionListener() {
                    Object[] possibilities = {"base product", "composite product"};
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String s = (String)JOptionPane.showInputDialog(a.getFrame(), "Enter the type of the menu item you want to modify:\n",
                                "Type of menu item", JOptionPane.PLAIN_MESSAGE, null, possibilities, "");
                        if(s != null){
                            if (s.equals("base product")){
                                String ss = (String)JOptionPane.showInputDialog(a.getFrame(), "Enter the name of the menu item you want to modify:\n",
                                        "Name of menu item", JOptionPane.PLAIN_MESSAGE, null, null, "");
                                if(ss != null){
                                    EditBaseProduct eBase = new EditBaseProduct(ss);
                                    eBase.getSave().addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            MenuItem itemBase = admin.editMenuItem(ss);
                                            double price = Double.parseDouble(eBase.getP().getText());
                                            itemBase.setPrice(price);
                                            r.editMenuItem(ss, itemBase);
                                            JOptionPane.showMessageDialog(a.getFrame(), "Item modified");
                                            r.getSerializator().serialize(r.getMeniu(), fileName);
                                        }
                                    });
                                }
                            }else{
                                String ss = (String)JOptionPane.showInputDialog(a.getFrame(), "Enter the name of the menu item you want to modify:\n",
                                        "Name of menu item", JOptionPane.PLAIN_MESSAGE, null, null, "");
                                if(ss != null){
                                    EditCompositeProduct eComp = new EditCompositeProduct(ss);
                                    eComp.getSave().addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            String arr = eComp.getI().getText();
                                            String[] arrOfStr = arr.split(", ");
                                            ArrayList<BaseProduct> ingredients = new ArrayList<>();
                                            for(String aux : arrOfStr){
                                                BaseProduct bP = (BaseProduct) r.findByName(aux);
                                                if(bP == null){
                                                    JOptionPane.showMessageDialog(a.getFrame(),
                                                            "Error! Enter only ingredients from the menu");
                                                    break;
                                                }
                                                else{
                                                    ingredients.add(bP);
                                                }
                                                if(ingredients.size()== arrOfStr.length){

                                                    MenuItem menuNew = new CompositeProduct(ss, ingredients);
                                                    r.editMenuItem(menuNew.getName(), menuNew);
                                                    JOptionPane.showMessageDialog(a.getFrame(), "Item modified");
                                                    r.getSerializator().serialize(r.getMeniu(), fileName);
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }

                    }
                });

                a.getListButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ListMenuItems it = new ListMenuItems(r);
                    }
                });
            }
        });

        control.getWaiterButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WaiterGraphicalUserInterface w = new WaiterGraphicalUserInterface();

                w.getCreateButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        NewOrder ord = new NewOrder();
                        ord.getSave().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                int id = Integer.parseInt(ord.getTextId().getText());
                                String date = ord.getTextDate().getText();
                                int table = Integer.parseInt(ord.getTextTable().getText());
                                String arr = ord.getTextDishes().getText();
                                String[] arrOfStr = arr.split(", ");
                                ArrayList<MenuItem> dishes = new ArrayList<>();
                                for(String aux : arrOfStr){
                                    MenuItem i = r.findByName(aux);
                                    if(i != null){
                                        dishes.add(i);
                                    }
                                }
                                if(dishes.size() > 0){
                                    r.addOrder(id, date, table, dishes);
                                    System.out.println(r.getMap());
                                }
                                else{
                                    JOptionPane.showMessageDialog(w.getFrame(), "The items you entered are not in the menu. Please try again\n",
                                            "Wrong data!", JOptionPane.PLAIN_MESSAGE, null);
                                }
                            }
                        });
                    }
                });

                w.getPriceButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ComputePriceForOrder c = new ComputePriceForOrder();
                        c.getSave().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                int idOrder = Integer.parseInt(c.getTextId().getText());
                                c.getTextTotal().setText(r.getWaiter().computeOrderPrice(r.findOrder(r.findOrderById(idOrder)))+"");
                            }
                        });
                    }
                });

                w.getBillButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String s = (String)JOptionPane.showInputDialog(w.getFrame(), "Enter the id of the order you want to bill:\n",
                                "ID of order", JOptionPane.PLAIN_MESSAGE, null, null, "");
                        if(s != null){
                            r.getWaiter().generateBill(Integer.parseInt(s), r);
                            JOptionPane.showMessageDialog(w.getFrame(), "The bill for order has been generated\n",
                                    "Bill information", JOptionPane.PLAIN_MESSAGE, null);
                        }
                        else{
                            JOptionPane.showMessageDialog(w.getFrame(), "The order ID you entered is not valid. Please try again\n",
                                    "Wrong data!", JOptionPane.PLAIN_MESSAGE, null);
                        }
                    }
                });

                w.getListButton().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ListOrders ord = new ListOrders(r);
                    }
                });
            }
        });
    }

    public static void main(String[] args) {

        MainControl start = new MainControl(args[0]);

    }
}
