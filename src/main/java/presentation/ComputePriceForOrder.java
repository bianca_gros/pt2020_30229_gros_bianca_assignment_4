package presentation;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class ComputePriceForOrder {
    private JFrame frame;
    private JPanel panel;
    private JPanel panelId;
    private JPanel panelTotal;
    private JLabel labelInformation;
    private JLabel labelId;
    private JLabel labelTotal;
    private JTextField textId;
    private JTextField textTotal;
    private JButton save;

    public ComputePriceForOrder(){
        this.frame = new JFrame();
        this.panel = new JPanel();
        this.panelId = new JPanel();
        this.panelTotal = new JPanel();
        this.labelInformation = new JLabel("Enter order information to get the order total");
        this.labelId = new JLabel("Order ID: ");
        this.labelTotal = new JLabel("Total is: ");
        this.textId = new JTextField(12);
        this.textTotal = new JTextField(12);
        this.save = new JButton("Get Total");

        panelId.add(labelId);
        panelId.add(textId);
        panelId.add(save);
        panelId.setLayout(new GridBagLayout());

        panelTotal.add(labelTotal);
        panelTotal.add(textTotal);
        panelTotal.setLayout(new GridBagLayout());

        panel.add(panelId);
        panel.add(panelTotal);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.add(panel);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JPanel getPanelId() {
        return panelId;
    }

    public void setPanelId(JPanel panelId) {
        this.panelId = panelId;
    }

    public JPanel getPanelTotal() {
        return panelTotal;
    }

    public void setPanelTotal(JPanel panelTotal) {
        this.panelTotal = panelTotal;
    }

    public JLabel getLabelInformation() {
        return labelInformation;
    }

    public void setLabelInformation(JLabel labelInformation) {
        this.labelInformation = labelInformation;
    }

    public JLabel getLabelId() {
        return labelId;
    }

    public void setLabelId(JLabel labelId) {
        this.labelId = labelId;
    }

    public JLabel getLabelTotal() {
        return labelTotal;
    }

    public void setLabelTotal(JLabel labelTotal) {
        this.labelTotal = labelTotal;
    }

    public JTextField getTextId() {
        return textId;
    }

    public void setTextId(JTextField textId) {
        this.textId = textId;
    }

    public JTextField getTextTotal() {
        return textTotal;
    }

    public void setTextTotal(JTextField textTotal) {
        this.textTotal = textTotal;
    }

    public JButton getSave() {
        return save;
    }

    public void setSave(JButton save) {
        this.save = save;
    }
}
