package data;

import business.MenuItem;
import business.Restaurant;

import java.io.*;
import java.util.ArrayList;

public class RestaurantSerializator{
    ArrayList<MenuItem> meniu;

    public void serialize(ArrayList<MenuItem> m, String fileName){
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(m);
            System.out.println("S-a facut serializarea");
            out.close();
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public ArrayList<MenuItem> deserialize(String fileName){
        try {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            meniu = (ArrayList<MenuItem>) in.readObject();
            System.out.println("S-a facut deserializarea");
            in.close();
            fileIn.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return meniu;
    }
}
