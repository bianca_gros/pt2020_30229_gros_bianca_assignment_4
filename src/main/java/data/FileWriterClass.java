package data;

import business.Order;
import business.Restaurant;

import java.io.IOException;

public class FileWriterClass {
    public FileWriterClass(){

    }

    public void write(Order com, Restaurant r){
        java.io.FileWriter fw = null;
        try {
            fw = new java.io.FileWriter("bill.txt");
            fw.write("Bill no. " + com.getOrderId());
            fw.write("\nDate of order: " + com.getOrderDate());
            fw.write("\nTable no. " + com.getTable());
            fw.write("\nServed dishes: " + r.getMap().get(com).toString());
            fw.write("\nTotal price: " + r.getWaiter().computeOrderPrice(r.getMap().get(com))+"");
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
